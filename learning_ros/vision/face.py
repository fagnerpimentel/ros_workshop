#!/usr/bin/env python

import rospy

import cv2
from cv_bridge import CvBridge, CvBridgeError

from sensor_msgs.msg import Image
from geometry_msgs.msg import Point

cascade_classifier = rospy.get_param('face/cascade_classifier')

class People:
    """docstring for people"""
    def __init__(self):
        
        self.rate = rospy.Rate(10)

        self.bridge = CvBridge()
        self.faces = []
        self.image = None
        
        self.size = -1
        
        self.face_cascade = cv2.CascadeClassifier(cascade_classifier)

        rospy.Subscriber('/usb_cam/image_raw', Image, self.callback)
        self.pub = rospy.Publisher('/face_position', Point , queue_size=10)
        rospy.spin()
                    

    def callback(self, data):
        self.image = self.bridge.imgmsg_to_cv2(data, 'bgr8')

        self.gray = cv2.cvtColor(self.image, cv2.COLOR_BGR2GRAY)
        self.faces = self.face_cascade.detectMultiScale(
            self.gray, 
            scaleFactor=1.1, 
            minNeighbors=5, 
            minSize=(100, 100), 
        flags=cv2.cv.CV_HAAR_SCALE_IMAGE)

        if (len(self.faces) > 0):
            self.size = None
            for number, (x, y, w, h) in enumerate(self.faces):
                x1 = x + int(w * .1)
                x2 = x1 + int(w * .8)

                y1 = y + int(h * .2)
                y2 = y1 + int(h * .8)

                roi_gray = self.gray[y1:y2, x1:x2]
                cv2.rectangle(self.image, (x1, y1), (x2, y2), (0, 255, 0), 2)
                size = w * h
                if size > 45000 and self.size < size:
                    self.size = size
                    self.maior = number
                    self.centrox = x1 + (x2 - x1) / 2
                    self.centroy = y1 + (y2 - y1) / 2
                    
                    p = Point()
                    p.x = self.centrox
                    p.y = self.centroy
                    self.pub.publish(p)
                    self.rate.sleep()

        cv2.waitKey(3)
        cv2.imshow('Faces', self.image)

if __name__ == "__main__":
    rospy.init_node('face')
    try:
        People()
    except KeyboardInterrupt:
        print("Shutting down")

