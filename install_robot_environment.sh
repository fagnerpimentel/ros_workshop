#################### ROS ####################
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
sudo apt-get update
sudo apt-get -y install ros-kinetic-desktop-full
sudo rosdep init
rosdep update
echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
source ~/.bashrc
sudo apt-get -y install python-rosinstall

#################### modules ####################
sudo apt-get -y install ros-kinetic-gazebo-ros
sudo apt-get -y install ros-kinetic-teleop-twist-keyboard
sudo apt-get -y install ros-kinetic-map-server
sudo apt-get -y install ros-kinetic-gmapping 
sudo apt-get -y install ros-kinetic-amcl
sudo apt-get -y install ros-kinetic-move-base
